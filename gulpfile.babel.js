'use strict';

import gulp from 'gulp';
import autoprefixer from 'gulp-autoprefixer';
import livereload from 'gulp-livereload';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import gutil from 'gulp-util';

import babelify from 'babelify';
import browserify from 'browserify';
import exorcist from 'exorcist';
import source from 'vinyl-source-stream';
import uglifyify from 'uglifyify';
import watchify from 'watchify';

const defaultDest = 'build/';
const bundles = [
	{
		input: ['js/main.js'],
		output: 'main.js',
	},
];

const createBundle = ({ input, output, dest = defaultDest }) => {
	const bundler = watchify(browserify({
		entries: input,
		debug: true,
		cache: {},
		packageCache: {},
		fullPaths: true,
	}));

	const build = (file) => {
		if (file) gutil.log(`Rebuilding ${file}`);
		return bundler
			.transform(babelify)
			.transform({ global: true }, uglifyify)
			.bundle()
			.on('error', gutil.log.bind(gutil, 'Browserify error'))
			.pipe(exorcist(`${dest}${output}.map`))
			.pipe(source(output))
			.pipe(gulp.dest(dest))
			.pipe(livereload());
	};

	bundler.on('update', build);
	return build();
};

gulp.task('watchify', () => {
	bundles.forEach(bundle => {
		createBundle(bundle);
	});
});

gulp.task('styles', () => {
	gulp.src('scss/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass.sync({ outputStyle: 'compressed' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(defaultDest))
		.pipe(livereload());
});

gulp.task('default', ['watchify', 'styles'], () => {
	livereload.listen();
	gulp.watch('scss/**/*.scss', ['styles']);
});
